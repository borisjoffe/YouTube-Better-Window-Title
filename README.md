Youtube Better Window Title
=============================

Features
--------
- Add video length in minutes (rounded) and Channel Name to Window Title to browse through your youtube tabs more easily
- When double-clicking on the video date, copy a wiki link (compatible with zim-wiki) to the clipboard to make it easier to add video references in PIM system

Requires
--------
One of the following:
- [TamperMonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=en) with [Brave](https://brave.com/) or [Chrome](https://www.google.com/chrome/browser/)
- [GreaseMonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/) with [Firefox](https://www.mozilla.org/firefox)
- [ViolentMonkey](https://addons.opera.com/en/extensions/details/violent-monkey/) / [TamperMonkey](https://addons.opera.com/en/extensions/details/tampermonkey-beta/?display=en) with [Opera](http://www.opera.com/) - untested

License
-------
MIT
